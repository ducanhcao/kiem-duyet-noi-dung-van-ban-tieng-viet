FROM python:3.11

ENV DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED=True \
    PORT=9090

# Install dependencies
RUN apt-get update \
    && apt-get install -y git 

WORKDIR /src
COPY ./server_manage_data/requirements.txt /src/
RUN pip install --no-cache-dir -r requirements.txt
COPY ./server_manage_data/*.py /src/

CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "8001"]