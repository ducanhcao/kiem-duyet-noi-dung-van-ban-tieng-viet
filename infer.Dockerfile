FROM pytorch/pytorch:2.3.1-cuda11.8-cudnn8-runtime

ENV DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED=True \
    PORT=9090

# Install dependencies
RUN apt-get update \
    && apt-get install -y git default-jre default-jdk

WORKDIR /src
RUN git clone https://github.com/vncorenlp/VnCoreNLP.git
RUN git clone https://huggingface.co/vinai/phobert-base/
COPY ./phobert-base/pytorch_model.bin /src/phobert-base/pytorch_model.bin 
COPY ./server_infer/requirements.txt /src/requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
COPY ./server_infer/*.py /src/

CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "8001"]