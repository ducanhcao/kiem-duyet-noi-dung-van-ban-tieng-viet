import logging
import logging.handlers
import sys

from constants import LOG_FILE

# Định nghĩa hàm để cấu hình logger
def configure_logger(log_file=LOG_FILE):
    # Tạo logger
    logger = logging.getLogger('NLP_Training')
    logger.setLevel(logging.DEBUG)

    # Định nghĩa handler để ghi log vào file
    file_handler = logging.FileHandler(log_file)
    file_handler.setLevel(logging.DEBUG)

    # Định nghĩa handler để in log ra stderr
    stream_handler = logging.StreamHandler(sys.stderr)
    stream_handler.setLevel(logging.DEBUG)

    # Định nghĩa format cho log
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    # Thêm handler vào logger
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger

# Sử dụng hàm để cấu hình logger
logger = configure_logger()
