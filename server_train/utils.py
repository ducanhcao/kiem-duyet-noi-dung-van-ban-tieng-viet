import numpy as np
import torch
import yaml 

def get_data_from_yaml(filename):
    try:
        with open(filename, 'r') as f:
            data = yaml.safe_load(f)
    except IOError:
        raise IOError(f"Error opening file: {filename}")

    return data

def seed_everything(seed_value):
    np.random.seed(seed_value)
    torch.manual_seed(seed_value)
    
    if torch.cuda.is_available(): 
        print("Torch available. Start seed_everything.")
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = True 
