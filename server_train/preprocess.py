import py_vncorenlp

from utils import get_data_from_yaml

config = get_data_from_yaml("/src/config.yaml")
CLASSES = config.get("classes")
VNCORENLP_DIR = config.get("vncorenlp")["save_dir"]

# Load the word and sentence segmentation component
rdrsegmenter = py_vncorenlp.VnCoreNLP(annotators=['wseg'], save_dir=VNCORENLP_DIR)

def preprocess_row(row):
    # Kiểm tra xem label có hợp lệ không 
    if row['label'] in CLASSES:
        content = rdrsegmenter.word_segment(row['text'])
        content = ''.join(content)
        return content
    else:
        # Nếu label không hợp lệ, thay đổi text
        return None