# Kiểm duyệt nội dung văn bản tiếng Việt

## Định nghĩa các nhãn phân loại

- **Phản động:** Trong lịch sử Việt Nam, "phản động" thường được dùng để mô tả các lực lượng hoặc cá nhân chống lại chính quyền cách mạng hoặc chế độ xã hội chủ nghĩa. <br>
Ở các quốc gia khác, từ "phản động" có thể được dùng để mô tả những người hoặc nhóm người chống lại sự thay đổi hoặc tiến bộ xã hội, nhằm duy trì trật tự hoặc hệ thống cũ.
- **Thù ghét:** Là những từ ngữ hoặc cụm từ có thể gây khó chịu, xúc phạm, hoặc tổn thương người nghe hoặc người đọc. <br>
Bao gồm các từ chửi thề, nói tục, hoặc bất kỳ từ ngữ nào có mục đích làm tổn thương hoặc hạ thấp người khác, những lời lẽ kích động bạo lực hoặc thù hằn đối với một nhóm người hoặc cá nhân cụ thể, các từ ngữ thể hiện sự khinh miệt, coi thường, hoặc thiếu tôn trọng đối với người khác. 
- **Khiêu dâm:** Là những tài liệu hoặc văn bản chứa các mô tả, hình ảnh, hoặc thông tin có mục đích kích thích tình dục một cách trực tiếp và rõ ràng. <br>
Nội dung thường chứa các mô tả cụ thể, rõ ràng về các hành động tình dục, bao gồm cả các chi tiết về cơ thể và hành vi tình dục.
- **Khác:** Không nằm trong các nhãn trên. 

## Dataset
https://drive.google.com/drive/folders/13EC69eegzPDL4ZCUSsbf-OPf4cLNerif?usp=sharing <br>
Các file dữ liệu sẵn sàng cho huấn luyện ở định dạng *.csv gồm 2 cột: <br>
- text: dữ liệu văn bản
- label: nhãn phân loại, chữ thường không dấu, phân cách từ bằng dấu "_"

## Hướng dẫn triển khai nhanh

### 1. Download model pretrain
Download pytorch-model.bin: https://huggingface.co/vinai/phobert-base/resolve/main/pytorch_model.bin?download=true <br>
then save in **./phobert-base/pytorch_model.bin**

### 2. Build docker images
Server inference
```
docker build -f infer.Dockerfile -t vn-text-moderation .
```
Server training
```
docker build -f train.Dockerfile -t vn-text-moderation-train .
```
Server manage data
```
docker build -f data.Dockerfile -t vn-text-moderation-data .
```

### 3. Run
```
docker compose up
``` 

## Danh sách API
### 1. API phân loại văn bản
**Request** <br>
*Giới hạn 10000 ký tự. Điều chỉnh trong file config.yaml: limit_infer_length*
```
curl -X POST http://10.3.2.100:8001/text-classify \
     -H "Content-Type: application/json" \
     -d '{
           "paragraph": "Cũng từ thời điểm này, luật Lực lượng tham gia bảo vệ ANTT ở cơ sở chính thức có hiệu lực. Không chỉ tại TP.HCM, cùng ngày, các địa phương trên cả nước đồng loạt ra mắt lực lượng này. Đây là lực lượng được kiện toàn từ 3 lực lượng: bảo vệ dân phố, công an xã bán chuyên trách và đội trưởng, đội phó dân phòng.\nThì ra hắn lật tới trang 17 của cuốn sách của công ty Remax hắn bắt gặp nhỏ Mỹ Hạnh, nhỏ này xinh ghê, nhìn mặt mày sáng sủa nhưng nhìn là biết dâm rồi, chân mày con nhỏ đậm ghê, trời trời nó chụp hình mà còn lòi cái vùng trăng trắng ở ngực ra nữa chứ. Điệu này là chết ngắt với thằng quỷ Cường này rồi. Kể ra thì cũng tội nghiệp nó, nó mới chia tay với con bồ Khánh Ly mới có 1 tuần mà gặp đàn bà con gái là nó thèm rỏ dãi. Nhớ cái thứ Bảy tuần rồi hắn đi chợ 88 nhìn cái mông lắc lắc của con bé mới có 20 mà tâm hồn hắn tê dại, hắn muốn nhào tới bóp mông con quỷ sứ khêu gợi 1 cái cho bỏ ghét nhưng hắn tự kiềm chế. Sống ở cái xứ Canada quỷ này, chạm ba cái vùng cấm đó là bị thưa như chơi. Nó tức nó tại bữa nọ lớn tiếng với con bồ vì lỡ tay làm bể cái chậu cá yêu qúy của nó, làm con nhỏ tội nghiệp giận nó không thèm tới nhà nó cho… nó đụ nữa.\nTạm dừng dùng Facebook làm gì anh ơi... , dm mặc kệ miệng lưỡi thiên hạ họ nói gì thì nói , đâu phải ai cũng là người trong cuộc đâu . Vì vậy anh hãy tiếp tục sử dụng Facebook đi, để còn biết trên Facebook anh đang bị cđm chửi sml chứ"
         }'
```
**Response** <br>
Status 200 OK
```
[
{
"chunk":"Cũng từ thời điểm này, luật Lực lượng tham gia bảo vệ ANTT ở cơ sở chính thức có hiệu lực . Không chỉ tại TP.HCM, cùng ngày, các địa phương trên cả nước đồng loạt ra mắt lực lượng này .",
"label":"khac",
"confidence":5.4834747314453125
},
{
"chunk":"Đây là lực lượng được kiện toàn từ 3 lực lượng: bảo vệ dân phố, công an xã bán chuyên trách và đội trưởng, đội phó dân phòng .",
"label":"khac",
"confidence":1.2335361242294312
},
{
"chunk":"Thì ra hắn lật tới trang 17 của cuốn sách của công ty Remax hắn bắt gặp nhỏ Mỹ Hạnh, nhỏ này xinh ghê, nhìn mặt mày sáng sủa nhưng nhìn là biết dâm rồi, chân mày con nhỏ đậm ghê, trời trời nó chụp hình mà còn lòi cái vùng trăng trắng ở ngực ra nữa chứ .",
"label":"khieu_dam",
"confidence":7.714803218841553
},
{
"chunk":"Điệu này là chết ngắt với thằng quỷ Cường này rồi . Kể ra thì cũng tội nghiệp nó, nó mới chia tay với con bồ Khánh Ly mới có 1 tuần mà gặp đàn bà con gái là nó thèm rỏ dãi .",
"label":"thu_ghet",
"confidence":6.8846821784973145
},
{
"chunk":"Nhớ cái thứ Bảy tuần rồi hắn đi chợ 88 nhìn cái mông lắc lắc của con bé mới có 20 mà tâm hồn hắn tê dại, hắn muốn nhào tới bóp mông con quỷ sứ khêu gợi 1 cái cho bỏ ghét nhưng hắn tự kiềm chế .",
"label":"khieu_dam",
"confidence":10.557592391967773
},
{
"chunk":"Sống ở cái xứ Canada quỷ này, chạm ba cái vùng cấm đó là bị thưa như chơi . Nó tức nó tại bữa nọ lớn tiếng với con bồ vì lỡ tay làm bể cái chậu cá yêu qúy của nó, làm con nhỏ tội nghiệp giận nó không thèm tới nhà nó cho… nó đụ nữa .",
"label":"khieu_dam",
"confidence":10.554232597351074
},
{
"chunk":"Tạm dừng dùng Facebook làm gì anh ơi.. . , dm mặc kệ miệng lưỡi thiên hạ họ nói gì thì nói , đâu phải ai cũng là người trong cuộc đâu . Vì vậy anh hãy tiếp tục sử dụng Facebook đi, để còn biết trên Facebook anh đang bị cđm chửi sml chứ",
"label":"thu_ghet",
"confidence":6.883141994476318
}
]
```
Status 400 Bad Request
```
{
    "detail": "Max length: 10000"
}
```
Status 500 Internal server error
```
{
    "detail": "detail of error"
}
```
### 2. API bổ sung dữ liệu đã gắn nhãn
**Request**
```
curl -X POST http://10.3.2.100:8008/add-data \
     -H "Content-Type: application/json" \
     -d '{
           "paragraph": "Cũng từ thời điểm này, luật Lực lượng tham gia bảo vệ ANTT ở cơ sở chính thức có hiệu lực. Không chỉ tại TP.HCM, cùng ngày, các địa phương trên cả nước đồng loạt ra mắt lực lượng này.",
           "label": "khac"
         }'

```
**Response** <br>
Status 200 OK
```
{
    "paragraph": "Cũng từ thời điểm này, luật Lực lượng tham gia bảo vệ ANTT ở cơ sở chính thức có hiệu lực. Không chỉ tại TP.HCM, cùng ngày, các địa phương trên cả nước đồng loạt ra mắt lực lượng này.",
    "label": "khac"
}
```
Status 500 Internal server error
```
{
    "detail": "detail of error"
}
```
### 3. API huấn luyện mô hình
**Request**
```
curl -X POST http://10.3.2.100:8000/start-training \
     -H "Content-Type: application/json" \
     -d '{
           "pretrain": "latest"
         }'
```
pretrain: 
- "" : huấn luyện model từ đầu
- "latest" : huấn luyện tiếp từ model huấn luyện gần nhất
- "model_name.pth" : huấn luyện tiếp từ model cụ thể 

**Response** <br>
Status 200 OK
```
{
    "status": "Training started",
    "message": "Tracking and visualizing metrics on TensorBoard UI: http://localhost:6006/"
}
```
Status 400 Bad Request
```
{
    "detail": "Training already in progress"
}
```
Status 500 Internal server error
```
{
    "detail": "detail of error"
}
```
### 4. API dừng huấn luyện mô hình
**Request**
```
curl -X POST http://10.3.2.100:8000/stop-training 
```
**Response** <br>
Status 200 OK
```
{
    "status": "Training stopped",
    "message": "VRam GPU are released"
}
```
Status 400 Bad Request
```
{
    "detail": "No training in progress"
}
```

## Các thành phần trong hệ thống
### MinIO
Quản lý model, dữ liệu huấn luyện.
### Postgres DB 
Lưu trữ dữ liệu đã gán nhãn từ người kiểm duyệt nội dung trên nền tảng MXH, được bổ sung liên tục để cải tiến mô hình.
### Adminer
Công cụ quản trị dữ liệu trên Postgres DB.
### NLP Data
Tự động kiểm tra và khởi tạo các bucket minio, các bảng trong cơ sở dữ liệu khi khởi động hệ thống. Cung cấp api bổ sung dữ liệu đã gắn nhãn vào cơ sở dữ liệu phục vụ cho cải tiến mô hình.
### NLP Infer
NLP Infer là thành phần chịu trách nhiệm xử lý và phân loại nội dung văn bản. Cung cấp điểm cuối (endpoint) để các hệ thống bên ngoài và các ứng dụng khác có thể sử dụng tính năng phân loại văn bản.
### Nginx
Thực hiện phân tải: phân phối yêu cầu của người dùng đến các NLP Core.
### NLP Training
NLP Training là thành phần thực hiện huấn luyện mô hình AI giúp cải tiến độ chính xác. Cung cấp điểm cuối (endpoint) để điều khiển (bắt đầu hoặc kết thúc) quá trình huấn luyện.
### Tensorboard
Giao diện theo dõi các chỉ số trong quá trình huấn luyện model.

## Cải tiến trong tương lai
Dữ liệu huấn luyện đang chưa đồng nhất: câu dài câu ngắn. 
Hiện tại khi đưa 1 đoạn văn vào suy luận, đoạn sẽ được chia thành các đoạn nhỏ theo chunk_size. Việc quyết định chunk_size là bao nhiêu phụ thuộc vào dữ liệu huấn luyện, càng gần với độ dài câu trong dữ liệu huấn luyện cho độ chính xác càng cao. 

