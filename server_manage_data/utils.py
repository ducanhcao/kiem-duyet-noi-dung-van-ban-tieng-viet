import yaml 

def get_data_from_yaml(filename):
    try:
        with open(filename, 'r') as f:
            data = yaml.safe_load(f)
    except IOError:
        raise IOError(f"Error opening file: {filename}")

    return data
