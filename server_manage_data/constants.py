from utils import get_data_from_yaml

config = get_data_from_yaml("/src/config.yaml")
DB_SERVER = config.get("sqldb")["server"]
DB_TABLENAME = config.get("sqldb")["table"]
MINIO_SERVER = config.get("minio")["server"]
MINIO_DATA_LABELED = config.get("minio")["data_labeled"]
MINIO_MODEL_TRAINED = config.get("minio")["model_trained"]
LOG_FILE = config.get("log_file")