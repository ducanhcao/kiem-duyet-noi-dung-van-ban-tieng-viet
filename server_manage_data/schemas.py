from pydantic import BaseModel

class LabeledDataCreate(BaseModel):
    paragraph: str
    label: str
