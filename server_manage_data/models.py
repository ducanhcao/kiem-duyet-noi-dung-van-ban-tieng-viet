from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

from constants import DB_TABLENAME

Base = declarative_base()

class LabeledData(Base):
    __tablename__ = DB_TABLENAME
    id = Column(Integer, primary_key=True, index=True)
    paragraph = Column(String, nullable=False)
    label = Column(String, nullable=False)
