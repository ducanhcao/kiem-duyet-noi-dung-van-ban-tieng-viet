import yaml 
import re

def get_data_from_yaml(filename):
    try:
        with open(filename, 'r') as f:
            data = yaml.safe_load(f)
    except IOError:
        raise IOError(f"Error opening file: {filename}")

    return data

def split_chunk(text, max_words=200):
    # Regular expression to match sentences and newlines
    sentence_endings = re.compile(r'([.!?])\s+|\n')
    
    # Split text into segments based on the regular expression
    segments = sentence_endings.split(text)
    
    paragraphs = []
    current_paragraph = []
    current_word_count = 0
    
    for segment in segments:
        if segment is not None:
            words = segment.split()
            word_count = len(words)
            
            # Check if adding this segment would exceed the max_words limit
            if current_word_count + word_count > max_words:
                # If so, finalize the current paragraph and start a new one
                paragraphs.append(' '.join(current_paragraph))
                current_paragraph = words
                current_word_count = word_count
            else:
                # Add the segment to the current paragraph
                current_paragraph.extend(words)
                current_word_count += word_count
    
    # Add the last paragraph if any
    if current_paragraph:
        paragraphs.append(' '.join(current_paragraph))
    
    return paragraphs
